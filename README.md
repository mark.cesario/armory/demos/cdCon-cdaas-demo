# cdCon-cdaas-demo
Get started with using CDaaS by deploying a sample application to your cluster.

## Using GitLab CI with Armory Continous Delivery as a Service and AWS EKS Cluster
1. Setup RNA using the docs here https://docs.armory.io/cd-as-a-service/setup/get-started/#prepare-your-deployment-target
2. Create new machine to machine credentials
Go to https://console.cloud.armory.io/configuration > Client Credentials
3. You can select the preconfigured scope group `Deployments using Spinnaker` or manually select the following:
```
manage:deploy
read:infra:data
exec:infra:op
read:artifacts:data
```
4. Copy the Client ID and Client Secret so that they can be saved as secrets later. 
5. Navigate to GitLab -> Settings -> CI/CD --> [Variables](
https://gitlab.com/mark.cesario/armory/demos/cdCon-cdaas-demo/-/settings/ci_cd)
6. Create 2 new secrets
   1. `CDAAS_CREDENTIAL_ID` and paste the value for Client ID created in Step 3 
   2. `CDAAS_CREDENTIAL_SECRET` and paste the value for Client Secret created in Step 4.
7. Create your AWS variables
   1. Enter you `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` variables.
8. Open `deploy.yml` and edit the `account` field to the name of the RNA. This can be found at https://console.cloud.armory.io/configuration > Agents (in the left side panel)
   `account: <your RNA name>`. Currently this is set to `my-first-cluster`
9. Commit and push the changes to the repository in the main branch. or run a pipeline.
10. Reivew your [pipeline](https://gitlab.com/mark.cesario/armory/demos/cdCon-cdaas-demo/-/pipelines) and open the deploy-cdaas job.  At the end of the jog log is a link to Armory Continuous Delivery as a Service link.

## Next Steps:
1. Copy the deploy.yml file in your GitLab repository.
2. Update the manifests, and namespace in deploy.yml to match your application.
3. Deploy your Application using CD-as-a-Service
4. Connect additional Kubernetes clusters if needed
